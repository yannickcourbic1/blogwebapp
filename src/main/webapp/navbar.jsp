<%@ page import="com.webapp.blogwebapp.Entities.User" %>
<%@ page import="com.webapp.blogwebapp.Utils.Utils" %>
<%@ page import="java.util.Arrays" %><%--
  Created by IntelliJ IDEA.
  User: Yanni
  Date: 13/10/2023
  Time: 11:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar bg-light fixed-top shadow">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">MesTutosWeb</a>
        <div class="d-none d-md-block d-lg-block">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ">
                <div class = "d-flex gap-3">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="home">
                            <i class="bi bi-house"></i>
                            Home
                        </a>
                    </li>
                <% if(session.getAttribute("email") != null){%>
                    <%
                        User userCurrent = (User) session.getAttribute("app_user");
                        System.out.println(Arrays.toString(userCurrent.getRoles()));
                    %>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="profil">
                            <i class="bi bi-person-circle"></i>
                            <%=session.getAttribute("email")%>
                        </a>
                    </li>
                        <% if(Utils.isAdmin(userCurrent)){%>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="admin">
                                    <i class="bi bi-person-fill-lock"></i>
                                    Administrateur
                                </a>
                            </li>
                        <%}%>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="disconnect">
                            <i class="bi bi-box-arrow-right"></i>
                            Déconnexion
                        </a>
                    </li>

                    <%}else{%>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="sign">
                                <i class="bi bi-person-plus-fill"></i>
                                S'inscrire
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="login">
                                <i class="bi bi-person-fill-check"></i>
                                Se Connecter
                            </a>
                        </li>
                    <%}%>
                </div>
            </ul>
        </div>
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Menu</h5>
                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/blogwebapp/home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
<%--                     <li class="nav-item dropdown"> --%>
<%--                         <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"> --%>
<%--                             Dropdown --%>
<%--                         </a> --%>
<%--                         <ul class="dropdown-menu"> --%>
<%--                             <li><a class="dropdown-item" href="#">Action</a></li> --%>
<%--                             <li><a class="dropdown-item" href="#">Another action</a></li> --%>
<%--                             <li> --%>
<%--                                 <hr class="dropdown-divider"> --%>
<%--                             </li> --%>
<%--                             <li><a class="dropdown-item" href="#">Something else here</a></li> --%>
<%--                         </ul> --%>
<%--                     </li> --%>
                </ul>
                <form class="d-flex mt-3" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </div>
</nav>