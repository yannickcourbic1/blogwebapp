<%@ page import="com.webapp.blogwebapp.Entities.User" %><%--
  Created by IntelliJ IDEA.
  User: Yanni
  Date: 17/10/2023
  Time: 19:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    User userCurrent = (User) session.getAttribute("app_user");
%>
<%if(request.getParameter("success") != null) {%>
    <% if(request.getParameter("user") != null) {%>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Veuillez vous connectez</strong></br>Vous avez enregistré avec succès
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <%}%>
    <% if(request.getParameter("profil") != null) {%>
    <div class="alert alert-success alert-dismissible fade show position-fixed " role="alert" style="margin-top: 5rem">
        <strong>Vous êtes connecté avec succès !</strong></br> Bienvenue <%= (userCurrent != null) ? userCurrent.getUsername() : ""%>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <%}%>
    <% if(request.getParameter("dis") != null) {%>
    <div class="alert alert-success alert-dismissible fade show position-fixed" role="alert" style="margin-top: 5rem">
        <strong>Vous êtes déconnecté avec succès !</strong></br>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <%}%>
    <% if(request.getParameter("formation") != null) {%>
    <div class="alert alert-success alert-dismissible fade show position-fixed" role="alert" style="margin-top: 5rem">
        <strong>Vous avez ajouté avec succès une formation !</strong></br>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <%}%>

<%}%>