<%--
  Created by IntelliJ IDEA.
  User: Yanni
  Date: 14/10/2023
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%if(request.getParameter("err") != null) {%>
    <% if(request.getParameter("empty") != null) {%>
        <div class="alert alert-warning alert-dismissible fade show position-fixed" role="alert">
            <strong>Attention !</strong></br> Vous n'avez pas rempli le formulaire correctement !
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <%}%>
    <% if(request.getParameter("email") != null) {%>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Erreur!</strong></br> Votre email n'est pas valide !
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <%}%>
    <% if(request.getParameter("plain") != null) {%>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Erreur!</strong></br> Les mots de passes ne sont pas identiques !
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <%}%>
    <% if(request.getParameter("pass") != null) {%>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Erreur!</strong></br>Votre mot de passe n'est pas assez fort !
            <ul>
                <li>Le mot de passe doit contenir au moins un chiffre [0-9]</li>
                <li>Le mot de passe doit contenir au moins un caractère latin minuscule [az]</li>
                <li>Le mot de passe doit contenir au moins un caractère latin majuscule [AZ]</li>
                <li>Le mot de passe doit contenir au moins un caractère spécial comme ! @ # & ( )</li>
                <li>Le mot de passe doit contenir au moins 8 caractères et au maximum 20 caractères.</li>
            </ul>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <%}%>
<%}%>