<%@ page import="java.util.List" %>
<%@ page import="com.webapp.blogwebapp.Entities.Formation" %>
<%@ page import="com.webapp.blogwebapp.Repository.FormationRepository" %><%--
  Created by IntelliJ IDEA.
  User: Yanni
  Date: 20/10/2023
  Time: 09:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../../../error.jsp"></jsp:include>
<jsp:include page="../../../success.jsp"></jsp:include>
<% FormationRepository formationRepository = new FormationRepository();%>
<div class="row d-flex justify-content-center">
    <h1 class="text-center text-primary overflow-hidden my-5">Ajouter Une Formation</h1>
    <div class="col-sm-11 col-md-6">
        <form method="post" action="lesson" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="title" class="form-label">Le titre de la leçon</label>
                <input type="text" class="form-control" id="title" name="title">
            </div>
            <div class="mb-3">
                <label for="videoUrl" class="form-label">Votre vidéo à upload</label>
                <input class="form-control" type="file" id="videoUrl" name="videoUrl">
            </div>
            <div class="form-floating">
                <textarea class="form-control" placeholder="décrire la formation" id="description" name="description" style="height: 100px"></textarea>
                <label for="description">Description</label>
            </div>
            <select class="form-select mt-4" aria-label="Default select example" name="formation">
                <option selected>Choisir une formation</option>
                <%for(Formation formation : formationRepository.findAll()){%>
                    <option value="<%=formation.getId()%>"><%=formation.getTitle()%></option>
                <%}%>
            </select>
            <button type="submit" class="btn btn-lg btn-primary text-center my-5">Ajouter</button>
        </form>
    </div>
</div>

