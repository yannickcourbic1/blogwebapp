<%--
  Created by IntelliJ IDEA.
  User: Yanni
  Date: 19/10/2023
  Time: 19:55
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="../../../error.jsp"></jsp:include>
<jsp:include page="../../../success.jsp"></jsp:include>
<div class="row d-flex justify-content-center">
    <h1 class="text-center text-primary overflow-hidden my-5">Ajouter Une Formation</h1>
    <div class="col-sm-11 col-md-6">
        <form method="post" action="formation" >
            <div class="mb-3">
                <label for="title" class="form-label">Le titre de la formation</label>
                <input type="text" class="form-control" id="title" name="title">
            </div>
            <div class="form-floating">
                <textarea class="form-control" placeholder="décrire la formation" id="description" name="description" style="height: 100px"></textarea>
                <label for="description">Description</label>
            </div>
            <button type="submit" class="btn btn-lg btn-primary text-center my-5">Ajouter</button>
        </form>
    </div>
</div>


