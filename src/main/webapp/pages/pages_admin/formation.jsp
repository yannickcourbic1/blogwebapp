<%--
  Created by IntelliJ IDEA.
  User: Yanni
  Date: 19/10/2023
  Time: 17:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<h2 class="text-center text-primary overflow-hidden">Les différentes formations</h2>
<hr class="text-white">
<ul class="nav nav-tabs w-100">
    <li class="nav-item overflow-hidden">
        <a class="nav-link <%=(request.getParameter("add") != null) ? "active" : ""%> " aria-current="page" href="<%=request.getContextPath()+"/admin?q=formation&add=1"%>"> <i class="bi bi-folder-plus text-primary fw-bold "></i> Ajouter une formation</a>
    </li>
    <li class="nav-item overflow-hidden">
        <a class="nav-link <%=(request.getParameter("update") != null) ? "active" : ""%>" href="<%=request.getContextPath()+"/admin?q=formation&update=1"%>"><i class="bi bi-folder-symlink text-warning fw-bold"></i> Modifier une formation</a>
    </li>
    <li class="nav-item overflow-hidden">
        <a class="nav-link <%=(request.getParameter("show") != null) ? "active" : ""%> " href="<%=request.getContextPath()+"/admin?q=formation&show=1"%>" > <i class="bi bi-folder-fill text-success fw-bold"></i> Voire en détail les différentes formations</a>
    </li>
    <li class="nav-item overflow-hidden">
        <a class="nav-link <%=(request.getParameter("remove") != null) ? "active" : ""%> " href="<%=request.getContextPath()+"/admin?q=formation&remove=1"%>"> <i class="bi bi-folder-x text-danger fw-bold"></i> Supprimer une formation</a>
    </li>
</ul>

<%if(request.getParameter("add") != null){%>
    <jsp:include page="./formation/add.jsp"></jsp:include>
<%}%>