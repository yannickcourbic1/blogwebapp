<%--
  Created by IntelliJ IDEA.
  User: Yanni
  Date: 19/10/2023
  Time: 09:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../header.jsp"></jsp:include>
<jsp:include page="../navbar.jsp"></jsp:include>
<jsp:include page="../warning.jsp"></jsp:include>

<div class="container mt-4">
    <div class="row" style="padding-top: 8rem">
        <!-- Barre latérale (Menu des onglets) -->
        <div class="col-md-2">
            <div class="list-group">
                <a href="admin" class="list-group-item list-group-item-action" ><i class="bi bi-house-lock-fill mx-2"></i> Dashboard</a>
                <a href="<%=request.getContextPath()+"/admin?q=formation"%>" class="list-group-item list-group-item-action"><i class="bi bi-bookmark-fill mx-2"></i> Formation</a>
                <a href="<%=request.getContextPath()+"/admin?q=lesson"%>" class="list-group-item list-group-item-action"><i class="bi bi-film mx-2"></i>Leçons</a>
                <a href="#" class="list-group-item list-group-item-action">Onglet 4</a>
            </div>
        </div>

        <!-- Contenu principal du dashboard -->
        <div class="col-md-10">
            <!-- Contenu de l'onglet sélectionné -->
            <%if(request.getParameter("q") != null){%>
                <%if(request.getParameter("q").equals("formation")) {%>
                    <jsp:include page="./pages_admin/formation.jsp"></jsp:include>
                <%}%>
                <%if(request.getParameter("q").equals("lesson")) {%>
                    <jsp:include page="./pages_admin/lesson.jsp"></jsp:include>
                <%}%>
            <%}%>
        </div>
    </div>
</div>

<style>
    *{
        overflow-x: hidden;
    }
</style>



<jsp:include page="../footer.jsp"></jsp:include>

