<%--
  Created by IntelliJ IDEA.
  User: Yanni
  Date: 13/10/2023
  Time: 15:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../header.jsp"></jsp:include>
<jsp:include page="../navbar.jsp"></jsp:include>

<div class="container col-xs-11 col-md-6 col-lg-4 border border-3 border-white p-5 rounded-3" style="margin-top: 5rem !important; background-color: rgba(241,241,241,0.25)">
    <h1 class="text-center text-primary">Inscription</h1>
    <jsp:include page="../error.jsp"></jsp:include>
    <form method="post" action="sign">
        <div class="mb-3">
            <label for="username" class="form-label">Votre Pseudo : </label>
            <input type="text" class="form-control" id="username" placeholder="test123" name="username">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Votre Email : </label>
            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="xyz@example.fr" name="email">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Votre mot de passe : </label>
            <input type="password" class="form-control" id="password" name="password" >
        </div>
        <div class="mb-3">
            <label for="plainPassword" class="form-label">Répétez votre mot de passe : </label>
            <input type="password" class="form-control" id="plainPassword" name="plainPassword">
        </div>
        <input type="submit" class="btn btn-lg btn-info text-white " value="s'enregistrer">
    </form>

</div>
<style>
    *{
        overflow: hidden;
    }
</style>
<jsp:include page="../footer.jsp"></jsp:include>
