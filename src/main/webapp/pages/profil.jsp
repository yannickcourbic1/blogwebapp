<%@ page import="com.webapp.blogwebapp.Entities.User" %><%--
  Created by IntelliJ IDEA.
  User: Yanni</input>
  Date: 18/10/2023
  Time: 10:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../header.jsp"></jsp:include>
<jsp:include page="../navbar.jsp"></jsp:include>

<%

        User userCustomer = (request.getSession(false) != null) ? (User) session.getAttribute("app_user") : null;
%>
<div class="container-fluid">

    <div class="container-profil">
        <jsp:include page="../success.jsp"></jsp:include>
        <div class="container-profil-detail d-flex gap-2  ">
            <img src="${pageContext.request.contextPath}/pages/images/img_default.png" alt="img de profil" class="container-img-profil">
            <div class="container-profil-hero">
                <h1>Mon Profil</h1>
                <button class="btn btn-sm btn-primary">Modifier mon image de profil</button>
            </div>
        </div>
        <hr class="text-white">
    </div>
    <div class="container-info container-sm">
        <h1 class="text-black mx-5"><i class="bi bi-person-lines-fill"></i> Mes Informations</h1>
        <hr class="text-white">
    </div>

    <div class="row">
        <form action="profil" method="post" class="d-flex justify-content-center align-items-center gap-3">
            <div class="col-sm-11 col-lg-3">
                <label for="username" class="form-label">Votre Pseudo</label>
                <input name="username" id="username"  type="text" value= "<%=(userCustomer.getUsername() != null) ? userCustomer.getUsername() : null%>" class="form-control bg-secondary text-white" placeholder="<%=(userCustomer.getUsername() != null) ? userCustomer.getUsername() : null%>">
            </div>


            <div class="col-sm-11 col-lg-3">
                <label for="email" class="form-label">Votre Email</label>
                <input name="email" id="email" type="email"  class="form-control bg-secondary text-white" value= "<%=(userCustomer.getEmail() != null) ? userCustomer.getEmail() : null%>">
            </div>
            <div class="col-sm-11 col-lg-3 mt-4">
                <button type="submit" class="btn btn btn-primary text-white" style="margin-top: 10px">Modifier vos informations</button>
            </div>
        </form>
    </div>
    <div class="container-info container-sm mt-5">
        <h1 class="text-black mx-5"><i class="bi bi-person-lines-fill"></i> Modifier votre mot de passe</h1>
        <hr class="text-white">
    </div>
    <div class="row">
        <form action="profil" method="post" class="d-flex justify-content-center align-items-center gap-3">
            <div class="col-sm-11 col-lg-3">
                <label for="password" class="form-label">Votre Mot de Passe : </label>
                <input name="password" id="password"  type="password" class="form-control bg-secondary text-white" >
            </div>


            <div class="col-sm-11 col-lg-3">
                <label for="plainPassword" class="form-label">Mot de passe à répéter </label>
                <input name="plainPassword" id="plainPassword" type="password"  class="form-control bg-secondary text-white" >
            </div>
            <div class="col-sm-11 col-lg-3 mt-4">
                <button type="submit" class="btn btn btn-primary text-white" style="margin-top: 10px">Modifier votre mot de passe</button>
            </div>

        </form>
    </div>

</div>

<jsp:include page="../footer.jsp"></jsp:include>