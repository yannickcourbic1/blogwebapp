<%--
  Created by IntelliJ IDEA.
  User: Yanni
  Date: 19/10/2023
  Time: 13:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%if(request.getParameter("warning") != null) {%>
<% if(request.getParameter("profil") != null) {%>
<div class="alert alert-warning alert-dismissible fade show position-fixed" role="alert" >
    <strong>Attention !</strong></br> Vous devez remplir vous connecter !
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<%}%>
<% if(request.getParameter("admin") != null) {%>
<div class="alert alert-warning alert-dismissible fade show position-fixed" role="alert" style="margin-top: 5rem;">
    <strong>Attention !</strong></br> Vous n'avez pas les droits nécessaires pour y accéder!
<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<%}%>
<%}%>
