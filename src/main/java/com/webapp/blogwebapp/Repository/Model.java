package com.webapp.blogwebapp.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.sql.SQLException;
import java.util.List;

public interface Model<T> {

     void create(T class1) throws JsonProcessingException;
     //T searchEmailExist(String email) throws SQLException;

     T update(T class1) throws SQLException;

     List<T> findAll() throws SQLException;
}
