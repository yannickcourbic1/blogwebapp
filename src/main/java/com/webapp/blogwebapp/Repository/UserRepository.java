package com.webapp.blogwebapp.Repository;
import java.sql.*;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webapp.blogwebapp.Data.DatabaseConnection;
import com.webapp.blogwebapp.Entities.User;

import java.time.LocalDateTime;
import java.util.List;

public class UserRepository extends DatabaseConnection implements Model<User> {

    private final Connection connection;

    public UserRepository(){
        this.connection = DatabaseConnection.getConnection();
    }
    @Override
    public void create(User class1) throws JsonProcessingException {
        //sérialiser le tableau en json
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(class1.getRoles());
        // Créez un objet LocalDateTime
        LocalDateTime localDateTime = LocalDateTime.now();

        // Utilisez le type de données TIMESTAMP pour insérer le LocalDateTime dans la base de données
        Timestamp timestamp = Timestamp.valueOf(localDateTime);
        String bcryptHashString = BCrypt.withDefaults().hashToString(12, class1.getPassword().toCharArray());

        try{
           String insertSQL = "insert into user (username,email,avatar,password,roles,createdAt) values (?,?,?,?,?,?)";
           PreparedStatement preparedStatement = this.connection.prepareStatement(insertSQL);
           preparedStatement.setString(1, class1.getUsername());
           preparedStatement.setString(2,class1.getEmail());
           preparedStatement.setString(3, class1.getAvatar());
           preparedStatement.setString(4,bcryptHashString);
           preparedStatement.setString(5, jsonString);
           preparedStatement.setTimestamp(6, timestamp);
           preparedStatement.execute();
       }catch (Exception e){
           e.printStackTrace();
       }
        finally {
            closeConnection(this.connection);
        }
    }

    @Override
    public User update(User class1) throws SQLException {
        return null;
    }

    @Override
    public List<User> findAll() throws SQLException {
        return null;
    }


    public User searchEmailExist(String email) throws SQLException {
        ObjectMapper objectMapper = new ObjectMapper();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        User u = null; // Vous n'avez pas besoin d'initialiser un objet User ici

        try {
            String selectSQL = "SELECT * FROM user WHERE email = ?";
            preparedStatement = connection.prepareStatement(selectSQL);
            preparedStatement.setString(1, email); // Paramètre la valeur de l'e-mail
            rs = preparedStatement.executeQuery();

            if (rs.next()) {
                // Utilisez les données de la ligne correspondante pour initialiser l'objet User
                u = new User();
                u.setId(rs.getLong("id"));
                u.setUsername(rs.getString("username"));
                u.setEmail(rs.getString("email"));
                u.setRoles(objectMapper.readValue(rs.getString("roles"), String[].class));
                u.setCreatedAt(rs.getTimestamp("createdAt").toLocalDateTime());
                u.setAvatar(rs.getString("avatar"));
                u.setPassword(rs.getString("password"));
                // Continuez avec d'autres colonnes

                System.out.println(u.getUsername());
            }
        } catch (SQLException | JsonProcessingException e) {
            e.printStackTrace();
        } finally {
            // Assurez-vous de fermer les ressources
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

        return u;
    }
}
