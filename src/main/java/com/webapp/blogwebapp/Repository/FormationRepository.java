package com.webapp.blogwebapp.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.webapp.blogwebapp.Data.DatabaseConnection;
import com.webapp.blogwebapp.Entities.Formation;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FormationRepository extends DatabaseConnection implements Model<Formation> {
    private final Connection connection;

    public FormationRepository(){
        this.connection = DatabaseConnection.getConnection();
    }
    public FormationRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void create(Formation class1) throws JsonProcessingException {
        LocalDateTime localDateTime = LocalDateTime.now();
        //crée une formation
        try {
            String insertSQL = "insert into formation (title,description,createdAt) values (?,?,?)";
            PreparedStatement preparedStatement = this.connection.prepareStatement(insertSQL);
            preparedStatement.setString(1, class1.getTitle());
            preparedStatement.setString(2, class1.getDescription());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(localDateTime));
            preparedStatement.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            closeConnection(this.connection);
        }
    }
        @Override
        public Formation update(Formation class1) throws SQLException {

            try {
                String insertSQL = "update formation set (title = '?' , description = '?', modifyAt = '?' with id = ?";
                PreparedStatement preparedStatement = this.connection.prepareStatement(insertSQL);
                preparedStatement.setString(1 , class1.getTitle());
                preparedStatement.setString(2 , class1.getDescription());
                preparedStatement.setTimestamp(3,Timestamp.valueOf(class1.getModifyAT()));
                preparedStatement.setLong(4,class1.getId());
                preparedStatement.execute();
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                closeConnection(this.connection);
            }
            return class1;
        }

    @Override
    public List<Formation> findAll() throws SQLException {

        ResultSet rs = null;
        List<Formation> formations = new ArrayList<Formation>();
        try {
            String insertSQL = "select * from formation";
            PreparedStatement preparedStatement = this.connection.prepareStatement(insertSQL);
            rs = preparedStatement.executeQuery();

            while (rs.next()){
                Formation formation = new Formation();
                formation.setId(rs.getLong("id"));
                formation.setTitle(rs.getString("title"));
                formation.setDescription(rs.getString("description"));
                formation.setCreatedAt(rs.getTimestamp("createdAt").toLocalDateTime());
                if(rs.getTimestamp("modifyAt") != null){
                    formation.setModifyAT(rs.getTimestamp("modifyAt").toLocalDateTime());
                }

                formations.add(formation);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            closeConnection(this.connection);
        }
        return formations;
    }


}
