package com.webapp.blogwebapp.Servlets;

import com.webapp.blogwebapp.Entities.User;
import com.webapp.blogwebapp.Repository.UserRepository;
import com.webapp.blogwebapp.Utils.Utils;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;
@WebServlet(name = "app_login", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    private UserRepository userRepository;

    public LoginServlet() {
        this.userRepository = new UserRepository();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(session != null && session.getAttribute("connection") != null){
            resp.sendRedirect("http://localhost:8080/blogwebapp/home");
        }
        else{
            req.getRequestDispatcher("/pages/login.jsp").forward(req, resp);

        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        if (email.isEmpty() || password.isEmpty()) {
            resp.sendRedirect("http://localhost:8080/blogwebapp/login?err=1&empty=1");
            return; // Arrêtez le traitement
        }

        String sanitizeEmail = Utils.sanitizeParam(email);
        String sanitizePassword = Utils.sanitizeParam(password);

        if (!Utils.validateEmail(sanitizeEmail)) {
            resp.sendRedirect("http://localhost:8080/blogwebapp/login?err=1&email=1");
            return; // Arrêtez le traitement
        }

        try {
            User userCurrent = this.userRepository.searchEmailExist(sanitizeEmail);

            if (userCurrent == null || !Utils.verifyPassword(sanitizePassword, userCurrent.getPassword())) {
                resp.sendRedirect("http://localhost:8080/blogwebapp/login?err=1&login=1");
                return; // Arrêtez le traitement
            }

            HttpSession session = req.getSession(true);
            session.setAttribute("app_user", userCurrent);
            session.setAttribute("connection", 1);
            session.setAttribute("email", userCurrent.getEmail());

            resp.sendRedirect("http://localhost:8080/blogwebapp/profil?success=1&profil=1");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}


