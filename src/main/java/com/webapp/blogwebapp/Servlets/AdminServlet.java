package com.webapp.blogwebapp.Servlets;

import com.webapp.blogwebapp.Entities.User;
import com.webapp.blogwebapp.Utils.Utils;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet(name = "app_admin" , urlPatterns = "/admin")
public class AdminServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);
        User userCurrent = (User) session.getAttribute("app_user");
        if(session == null ||  userCurrent == null || !Utils.isAdmin(userCurrent)){
            resp.sendRedirect("http://localhost:8080/blogwebapp/home?warning=1&admin=1");
        }
        else{
            req.getRequestDispatcher("/pages/admin.jsp").forward(req , resp);
        }
    }
}
