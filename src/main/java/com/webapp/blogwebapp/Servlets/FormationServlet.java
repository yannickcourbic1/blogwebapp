package com.webapp.blogwebapp.Servlets;

import com.webapp.blogwebapp.Entities.Formation;
import com.webapp.blogwebapp.Repository.FormationRepository;
import com.webapp.blogwebapp.Utils.Utils;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.LocalDateTime;

@WebServlet(name = "app_formation" , urlPatterns = "/formation")
public class FormationServlet extends HttpServlet {

    private FormationRepository formationRepository;
    private Formation formation;
    public FormationServlet(){
        this.formationRepository = new FormationRepository();
        this.formation = new Formation();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        super.doPost(req, resp);
        this.add(req , resp);

    }

    private void add( HttpServletRequest request , HttpServletResponse response) throws IOException {

        if(!request.getParameter("title").isEmpty() && !request.getParameter("description").isEmpty()){
            String title = request.getParameter("title").trim();
            String description = request.getParameter("description").trim();
            String sanitizeTitle = Utils.sanitizeParam(title);
            String sanitizeDescription = Utils.sanitizeParam(description);
            //si les champs ne sont pas vide
            LocalDateTime localDateTime = LocalDateTime.now();
            this.formation.setTitle(sanitizeTitle);
            this.formation.setDescription(sanitizeDescription);
            this.formation.setCreatedAt(localDateTime);
            this.formationRepository.create(this.formation);

            response.sendRedirect("http://localhost:8080/blogwebapp/admin?q=formation&add=1&success=1&formation=1");

        }
        else{
            response.sendRedirect("http://localhost:8080/blogwebapp/admin?q=formation&add=1&err=1&empty=1");

        }

    }


}
