package com.webapp.blogwebapp.Servlets;
//import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.BCrypt;
import com.webapp.blogwebapp.Entities.User;
import com.webapp.blogwebapp.Repository.UserRepository;
import com.webapp.blogwebapp.Utils.Utils;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
//import java.util.Arrays;


@WebServlet(name="sign" , urlPatterns = "/sign")
public class SignServlet extends HttpServlet {
    private UserRepository userRepository;

    public  SignServlet(){
        this.userRepository = new UserRepository();
    }
    protected  void processRequest(HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/pages/sign.jsp").forward(request , response);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);
        if(session != null && session.getAttribute("connection") != null){
            resp.sendRedirect("http://localhost:8080/blogwebapp/home");
        }
        else{
            processRequest(req , resp);
        }



    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Fetch form data
        String username = req.getParameter("username").trim();
        String email = req.getParameter("email").trim();
        String password = req.getParameter("password").trim();
        String plainPassword = req.getParameter("plainPassword").trim();
        // Check if any of the fields are empty
        if (username.isEmpty() || email.isEmpty() || password.isEmpty() || plainPassword.isEmpty()) {
            resp.sendRedirect("http://localhost:8080/blogwebapp/sign?err=1&empty=1");
            return; // Exit the method to avoid further processing
        }
        // Sanitize input data
        String sanitizeUsername = Utils.sanitizeParam(username);
        String sanitizeEmail = Utils.sanitizeParam(email);
        String sanitizePassword = Utils.sanitizeParam(password);
        String sanitizePlainPassword = Utils.sanitizeParam(plainPassword);
        // Check if the password is strong
        if (!Utils.strongPassword(sanitizePassword)) {
            resp.sendRedirect("http://localhost:8080/blogwebapp/sign?err=1&pass=1");
            return; // Exit the method
        }
        // Check if the email is valid
        if (!Utils.validateEmail(sanitizeEmail)) {
            resp.sendRedirect("http://localhost:8080/blogwebapp/sign?err=1&email=1");
            return; // Exit the method
        }
        // Check if password and plainPassword match
        if (!sanitizePassword.equals(sanitizePlainPassword)) {
            resp.sendRedirect("http://localhost:8080/blogwebapp/sign?err=1&plain=1");
            return; // Exit the method
        }
        try {
           //String hashPassword = Arrays.toString(BCrypt.with(BCrypt.Version.VERSION_2Y).hashToChar(6, sanitizePassword.toCharArray()));
            String[] str = {"ROLE_USER"};
            LocalDateTime d = LocalDateTime.now();
            User u = new User();
            u.setUsername(sanitizeUsername);
            u.setEmail(sanitizeEmail);
            u.setPassword(sanitizePassword);
            u.setCreatedAt(d);
            u.setRoles(str);
            this.userRepository.create(u);
        }catch (Exception e){
            e.printStackTrace();
        }

        resp.sendRedirect("http://localhost:8080/blogwebapp/login?success=1&user=1");
    }

}