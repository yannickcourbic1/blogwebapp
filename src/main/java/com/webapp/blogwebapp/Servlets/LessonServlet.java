package com.webapp.blogwebapp.Servlets;

import com.webapp.blogwebapp.Entities.Lesson;
import com.webapp.blogwebapp.Utils.Utils;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "app_lesson" , urlPatterns = "/lesson")
@MultipartConfig
public class LessonServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        this.add(req, resp);
    }

    private void add(HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException {

        if(request.getParameter("title") != null || request.getParameter("description") != null || request.getPart("videoUrl") != null && request.getParameter("formation") != null || request.getParameter("formation") != null){

            String sanitizeTitle = Utils.sanitizeParam(request.getParameter("title"));
            String sanitizeDescription = Utils.sanitizeParam(request.getParameter("description"));
            String fileVideoUrlPath = Utils.UploadVideo(request ,response ,Utils.currentDirectory() + "/src/main/webapp/pages/video/" , "videoUrl");
            String sanitizeId = Utils.sanitizeParam(request.getParameter("formation"));
            Lesson lesson = new Lesson();

        }
    }

}
