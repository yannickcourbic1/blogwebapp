package com.webapp.blogwebapp.Entities;

public class Lesson {

    public Long id;
    public String title;
    public String Description;

    public String videoUrl;

    public Formation formationID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Formation getFormationID() {
        return formationID;
    }

    public void setFormationID(Formation formationID) {
        this.formationID = formationID;
    }
}
