package com.webapp.blogwebapp.Entities;

import java.time.LocalDateTime;

public class Formation {
    public Long id;

    public String title;

    public String description;

    public LocalDateTime createdAt;

    public LocalDateTime modifyAT;

    public Formation(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getModifyAT() {
        return modifyAT;
    }

    public void setModifyAT(LocalDateTime modifyAT) {
        this.modifyAT = modifyAT;
    }

    @Override
    public String toString() {
        return "Formation{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", createdAt=" + createdAt +
                ", modifyAT=" + modifyAT +
                '}';
    }
}
