package com.webapp.blogwebapp.Data;

import com.mysql.cj.jdbc.ConnectionGroup;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public abstract class DatabaseConnection {

        private static final String JDBC_URL = "jdbc:mysql://localhost:3306/blogwebapp?useSSL=false&characterEncoding=UTF-8";
        private static final String JDBC_USER = "COURSDB";
        private static final String JDBC_PASSWORD = "Esao123?!";

        public static Connection getConnection() {
            try {
                // Charger le pilote JDBC (assurez-vous d'avoir le pilote correspondant à votre base de données dans le chemin de classe)
                Class.forName("com.mysql.cj.jdbc.Driver");

                // Établir la connexion
                Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);

                return connection;
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
                return null;
            }
        }

        public static void closeConnection(Connection connection) {

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
