package com.webapp.blogwebapp.Utils;
import at.favre.lib.crypto.bcrypt.BCrypt;
import com.webapp.blogwebapp.Entities.User;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils  {

    public static void fileUpload(){

    }

    private static Boolean matchScript(String params){
        Pattern pattern = Pattern.compile("<.*?>|&.*?;|%[0-9a-fA-F]*");
        Matcher m;
        m = pattern.matcher(params);
        return m.find();

    }

    public static Boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile("^[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
        Matcher m = pattern.matcher(email);
        return m.find();
    }


    public static Boolean strongPassword(String password) {
        Pattern pattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()\\[\\]{}:;',?/*~$^+=<>]).{8,20}$");
        Matcher m = pattern.matcher(password);
        return m.find();
    }


    public static String sanitizeParam(String params) {
        // Supprimer toutes les balises HTML et les caractères d'échappement
        String param = params.replaceAll("<.*?>|&.*?;|%[0-9a-fA-F]*", "");
        return param;
    }

    public static boolean verifyPassword(String password , String bcryptHashString){
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), bcryptHashString);
        return result.verified;
    }

    public static boolean isAdmin(User userCurrent) {
        String[] roles = userCurrent.getRoles();
        boolean isAdmin = false; // Initialisez à false par défaut

        for (String role : roles) {
            if (role.equals("ROLE_ADMIN")) {
                isAdmin = true;
                break; // Vous pouvez sortir de la boucle dès que vous avez trouvé un rôle admin
            }
        }

        return isAdmin;
    }

    public static String UploadVideo(HttpServletRequest request , HttpServletResponse response , String path , String name ){
        String filename = null;
        try {
            Part filePart = request.getPart(name);
             filename = getFileName(filePart);

            InputStream fileContent = filePart.getInputStream();

            OutputStream out = new FileOutputStream(new File(path));
            int read;
            final byte[] bytes = new byte[1024];

            while((read = fileContent.read(bytes)) != -1){
                out.write(bytes , 0 ,read);
            }
            out.close();


        }catch (Exception e){
            e.printStackTrace();
        }

        return filename;
    }

    private static String getFileName(final  Part part){
        for (String content : part.getHeader("content-disposition").split(";")){
            if(content.trim().startsWith("filename")){
                return content.substring(content.indexOf('=') + 1).trim().replaceAll("\"", "");
            }
        }
        return  "unknown";
    }

    public static String currentDirectory(){
        String directory = System.getProperty("user.div");
        String newDirectory = directory.replace("\\" , "/");
        return newDirectory;
    }


}
