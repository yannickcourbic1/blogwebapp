import com.webapp.blogwebapp.Data.DatabaseConnection;
import com.webapp.blogwebapp.Entities.Formation;
import com.webapp.blogwebapp.Entities.User;
import com.webapp.blogwebapp.Repository.FormationRepository;
import com.webapp.blogwebapp.Repository.UserRepository;
import jakarta.servlet.http.HttpSession;

import java.nio.file.FileStore;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectDatabase extends DatabaseConnection {

     public static void main(String[] args) throws SQLException, ClassNotFoundException {

//         UserRepository userRepository = new UserRepository();
//         System.out.println(userRepository.searchEmailExist("a.yacine2023@gmail.com").toString());

//         FormationRepository formationRepository = new FormationRepository();
//
//         for (Formation formation : formationRepository.findAll()){
//             System.out.println(formation.toString());
//         }
//
//         String currentDirectory = System.getProperty("user.dir");
//         System.out.println("Répertoire courant : " + currentDirectory);

         String directory = System.getProperty("user.dir");
         String newDirectory = directory.replace("\\" , "/");
         System.out.println(newDirectory);
     }
}
